#!/usr/bin/env bash

# helper function to test if string is in bash array
# input:
#   $1: string to look for
#   $2: bash array of strings
# output:
#   0 if array does contain string
#   1 if array doesn't contain string
check_parameter() {
    local parameter=${1}
    shift
    local list=${*}

    for p in ${list} ; do
        if [[ "${p}" == "${parameter}" ]] ; then
            return 0
        fi
    done
    return 1
}

