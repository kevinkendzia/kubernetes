#!/usr/bin/env bash

export HDD_DATA_MNT=/mnt/data
export SSD_DATA_MNT=/data

###########################################################################################
# java parameter stuff
###########################################################################################

export HEAP1G="-Xmx1g"
export HEAP2G="-Xmx2g"
export HEAP4G="-Xmx4g"
export HEAP6G="-Xmx6g"
export HEAP8G="-Xmx8g"
export HEAP12G="-Xmx12g"
export HEAP16G="-Xmx16g"