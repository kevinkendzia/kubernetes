#!/usr/bin/env bash

### source important files
source utils/default.bash
source utils/functions.bash

available_methods=(create apply delete describe)
if [[ ${#@} -lt 1 ]]; then
    echo "Too less arguments"
    echo "./deploy.sh <Method> <Target> <Stage> <Options>"
    echo "Method: create, apply, delete, describe"
    echo "Target: Directory of what to deploy (default: 'dev') - i.e. corpus/foolnews/dev or corpus/dev, dev"
    echo "Stage: dev, staging, prod (default: dev), but everything is possible (necessary for folder creation)"
    exit 1
fi

method=$1
shift
target=$1
shift
stage=$1

# check parameter
if [[ $(check_parameter ${method:?} ${available_methods[@]}; echo $?) -eq 1 ]]; then
    echo "Method ${method} not valid."
    echo "Options: ${available_methods[@]}"
    exit 1
fi
echo "Method: ${method}"
if [[ -z ${target} ]]; then
    target="dev"
fi
echo "Target: ${target}"
if [[ -z ${stage} ]]; then
    stage="dev"
fi
echo "Stage: ${stage}"

DRYRUN=""
while getopts "d" opt; do
  case $opt in
    d) DRYRUN='--dry-run' ;;
  esac
done

if [[ -z ${GIT_REVISION+x} ]]; then
    GIT_REVISION=$(git rev-parse HEAD)
    if [[ $(echo $?) -ne 0 ]]; then
        echo "Couldn't determine GIT_REVISION, using latest"
        GIT_REVISION=latest
    fi
fi

# create data directories
export GIT_REVISION
export HDD_DATA_DIRECTORY=${HDD_DATA_MNT}/artis_${stage}
export SDD_DATA_DIRECTORY=${SSD_DATA_MNT}/artis_${stage}
# mkdir -p ${HDD_DATA_DIRECTORY} ${SDD_DATA_DIRECTORY}

export NAMESPACE="artis-${stage}"
case ${method} in
    "create")
        echo "Create deployment"
        kubectl kustomize ${target} | envsubst | kubectl create -f -
        ;;
    "apply")
        echo "Apply deployment"
        kubectl kustomize ${target} | envsubst | kubectl apply -f -
        ;;
    "describe")
        kubectl kustomize ${target} | envsubst | kubectl describe -f -
        ;;
    "delete")
        echo "Delete deployment"
        kubectl kustomize ${target} | envsubst | kubectl delete -f -
        ;;
    *)
        echo "WHAT?"
        ;;
esac
